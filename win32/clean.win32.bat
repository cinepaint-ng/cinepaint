@ECHO Robin's Win32 clean-up batch file...

del win32\*.orig
del win32\*.rej

del win32\debug\libcinepaint.dll
del win32\debug\cinepaint.*


del win32\cinepaint\debug\* /q
del win32\cinepaint\release\* /q

del win32\libcinepaint\debug\* /q
del win32\libcinepaint\release\* /q

del win32\pkg_plug_in\debug\* /q
del win32\pkg_plug_in\release\* /q

del win32\plug-ins\debug\* /q
del win32\plug-ins\release\* /q

del win32\vcpp\debug\* /q
del win32\vcpp\release\* /q
del win32\vcpp\cinepaint.opt
# del win32\vcpp\cinepaint.ncb -- has debug settings!

rmdir app\Debug
rmdir app\Release
rmdir plug-ins\Debug
rmdir plug-ins\Release
rmdir lib\Debug
rmdir lib\Release
rmdir shm\Debug
rmdir shm\Release
rmdir wire\Debug
rmdir wire\Release
rmdir win32\Debug\Debug
rmdir win32\Debug\Release

rmdir win32\cinepaint\debug
rmdir win32\cinepaint\release

rmdir win32\libcinepaint\debug
rmdir win32\libcinepaint\release

rmdir win32\pkg_plug_in\debug
rmdir win32\pkg_plug_in\release

rmdir win32\plug-ins\debug
rmdir win32\plug-ins\release

rmdir win32\vcpp\debug
rmdir win32\vcpp\release

pause


