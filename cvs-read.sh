echo cvs-read.sh Robin.Rowe@CinePaint.org
echo License BSD 10.18.2008
echo INSTRUCTIONS: This enables you to read CVS, but not write.
echo If you are a member of the CinePaint team, you want cvs-dev.sh

mkdir cvs
cd cvs
echo Press [Enter] when prompted for password...
cvs -d:pserver:anonymous@cvs.sourceforge.net:/cvsroot/cinepaint login
cvs -z3 -d:pserver:anonymous@cinepaint.cvs.sourceforge.net:/cvsroot/cinepaint co cinepaint-project
cd cinepaint-project/cinepaint/
./autogen.sh 
./configure

