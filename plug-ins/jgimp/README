JGimp Plug-In, Version 0.8
==========================

An extension to the GIMP to write Java- and Jython-based plug-ins
and extensions. Plug-ins extend the GIMP's functionality, while
extensions use the GIMP as an image manipulation service.

Other files of interest
=======================
"Copyright" for copyright information.
"COPYING" for license information
"README.FilmGIMP" contains information for Cinepaint (previously
  called Film GIMP).
"manual.pdf" in the docs directory for information on JGimp, the 
  architecture, and how to write your own plug-ins and extensions

In this file
============
This file presents a quick overview of how to install JGimp for
UN*X platforms. For Windows-specific install information, see
the manual in the docs directory.

Requirements
============
- GIMP 1.2.3 or later
- GNU make and GCC
- Sun Java 1.4 or later
- Ant 1.5.1 or later (http://ant.apache.org)
- Optional: Jython 2.1 or later (http://www.jython.org)

Installation
============
1. Run ./configure, passing in any optional flags you desire.

   You will likely need to pass these options to ./configure:

   * --with-jdk-prefix=/path/to/java/JDK/install

      Use this option to specify where java is installed.  The path 
      should refer to the root of the JDK install, not the path 
      to the bin directory or the actual javac program.

      For example, if javac is located at /usr/java/j2sdk1.4.0_01/bin/javac,
      then you should use --with-jdk-path=/usr/java/j2sdk1.4.0_01

  * --prefix=/gimp/prefix

      You need to specify where the gimp is installed.  For example,
      if "which gimp" returns /usr/local/bin/gimp, you would use
      --prefix=/usr/local

  So with our examples, the correct command would be:

  ./configure --prefix=/usr/local/gimp --with-jdk-prefix=/usr/java/j2sdk1.4.0_01

1a. Jython support: If you want Jython support, you need jython.jar
    (the jar file that comes with Jython 2.1) in your CLASSPATH. You must
    also pass in the switch --with-jython to configure. So you might
    have something like:

  ./configure --prefix=/usr/local/gimp --with-jdk-prefix=/usr/java/j2sdk1.4.0_01 --with-jython

2. Run "make" and "make install"

3. Start up the GIMP! The sample Java-based plug-ins can be found by
   right-clicking on an image, and going to:

   Filters->Digital Cameras->Nikon Image Divider
   Filters->Colors->Desaturation Tools->Desaturation Dialog...
   Filters->Colors->Desaturation Tools->Extract red channel only
   ...

3a. If you compiled in support for Jython, make sure it is in your
    CLASSPATH when you execute GIMP, or place the jar file with the
    rest of JGimp's jar files (in GIMP/lib/gimp/1.2/jgimp/javafiles, where
    GIMP is the path to the GIMP base directory).
