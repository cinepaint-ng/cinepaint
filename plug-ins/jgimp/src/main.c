/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * Based on:
 * GIMP Plug-In Template
 * Copyright (C) 2000  Michael Natterer <mitch@gimp.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef JGIMP_COMPILE_FOR_FILMGIMP
#define GimpPlugInInfo GPlugInInfo
#endif /* JGIMP_COMPILE_FOR_FILMGIMP */

#ifndef _WIN32
 #include <unistd.h> /* for sleep */
#else
 #include <WINDOWS.H>
#endif /* _WIN32 */

#include <stdio.h>
#include <string.h>

#include <libgimp/gimp.h>

#include "jgimp_jni_utils.h"
#include "jgimp_constants.h"

#include "plugin-intl.h"


/* 
   External function prototypes 
   These are called to help install the extension
*/
extern void  gimp_extension_process (guint timeout);
extern void  gimp_extension_ack     (void);

/*  
    Local function prototypes
    These are called by the Gimp
*/
static void   init  (void);
static void   quit  (void);
static void   query (void);
static void   run   (gchar      *name,
		     gint        nparams,
		     GimpParam  *param,
		     gint       *nreturn_vals,
		     GimpParam **return_vals);


/*  
    Global Gimp variables  
    These are required for the Gimp to install the plugin properly.
*/
GimpPlugInInfo PLUG_IN_INFO =
{
  init,  /* init_proc  */
  quit,  /* quit_proc  */
  query, /* query_proc */
  run,   /* run_proc   */
};

#ifdef _WIN32
/*
  Got this snippet from http://www.planet-source-code.com/vb/scripts/ShowCode.asp?txtCodeId=4918&lngWId=3
  This code will prevent the console window from popping up.
  Comment this line out if you need debug messages
*/
/* #pragma comment( linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"" ) */
#endif /* _WIN32 */

/*  
    A macro defined in libgimp/gimp.h to intialize and setup communcations 
    links between the gimp and the plugin.
*/ 
MAIN ()


static void
init  (void)
{
	; /* no initialization done here */
}

static void
quit  (void)
{
	/* Tell JGimp to shutdown all plug-ins and extensions */
	jni_utils_shutdown_jgimp();
}

static void
query (void)
{
    /* STUB: add internationalization stuff later */
/*
	gimp_plugin_domain_register (PLUGIN_NAME, LOCALEDIR);
	gimp_plugin_help_register (DATADIR"/help");
*/

	/* This component handles plug-ins written in Java */
	gimp_install_procedure (JGIMP_PLUGIN_SERVER, /*  Constant defined in jgimp_constants.h */
			  "JGimp - A Java extension for the GIMP",
			  "Java extension for the GIMP to enable Java-based plug-ins.",
			  "Michael Terry <mterry@cc.gatech.edu>",
			  "Michael Terry <mterry@cc.gatech.edu>",
			  "2002",
			  NULL,
			  NULL,
			  GIMP_EXTENSION,
			  0, 0,
			  NULL, NULL);

	/* The server is the component that handles extensions written in Java */
	gimp_install_procedure (JGIMP_EXTENSION_SERVER, /*  Constant defined in jgimp_constants.h */
			  "JGimp Server - A Java extension for the GIMP",
			  "Java extension for the GIMP to enable Java-based extensions. This server enables Java code to use the GIMP as service",
			  "Michael Terry <mterry@cc.gatech.edu>",
			  "Michael Terry <mterry@cc.gatech.edu>",
			  "2002",
			  NULL,
			  NULL,
			  GIMP_EXTENSION,
			  0, 0,
			  NULL, NULL);
}

static void
run (gchar      *name, 
     gint        n_params, 
     GimpParam  *param, 
     gint       *unused_nreturn_vals,
     GimpParam **unused_return_vals)
{
	gchar* lib_dir                             = NULL;
	gchar* base_jgimp_dir                      = NULL;
	gchar* jgimp_jarfile_location              = NULL;
    int    is_plugin_server                    = (strcmp(name, JGIMP_PLUGIN_SERVER) == 0);
    int    result = 0;

#ifdef _WIN32

#ifdef FULL_CUSTOM_BUILD
	/* The path for our full custom installer that includes the GIMP */
	lib_dir = g_strdup("..\\lib\\gimp\\1.2\\");
#else
	/* This will build for installing with the standard Windows GIMP */
	lib_dir = g_strdup("..\\..\\..\\..\\GIMP\\lib\\gimp\\1.2\\");
#endif /* FULL_CUSTOM_BUILD */

	base_jgimp_dir = g_strdup_printf("%s\\jgimp", lib_dir);
	jgimp_jarfile_location = g_strdup_printf("%s\\javafiles\\jgimp.jar", base_jgimp_dir);
#else
	base_jgimp_dir = g_strdup(JGIMP_BASE_DIR);
	jgimp_jarfile_location = g_strdup_printf("%s/javafiles/jgimp.jar", base_jgimp_dir);
#endif /* _WIN32 */

	/*
	 * Load JGimp
	 */
    result = jni_utils_load_jgimp(jgimp_jarfile_location, base_jgimp_dir, ((is_plugin_server) ? JNI_TRUE : JNI_FALSE), ((is_plugin_server) ? JNI_FALSE : JNI_TRUE));

    if (result)
    {
#ifdef _WIN32
		char   full_path[1024];
		char*  filename_pointer;
#endif /* _WIN32 */
        fprintf(stderr, "Failed to load JVM when loading the JGimp server. Bailing out.\n");
#ifdef _WIN32
		if (GetFullPathName(jgimp_jarfile_location, 1024, full_path, &filename_pointer))
		{
			fprintf(stderr, "JGimp jarfile location specified: %s\n", full_path);
		}
		else
		{
			fprintf(stderr, "JGimp jarfile location specified: %s\n", jgimp_jarfile_location);
		}
#else
		fprintf(stderr, "JGimp jarfile location specified: %s\n", jgimp_jarfile_location);
#endif /* _WIN32 */
    }

    /* Free any memory that may have been allocated */
	if (lib_dir != NULL)
	{
		g_free(lib_dir);
		lib_dir = NULL;
	}
    if (base_jgimp_dir != NULL)
	{
		g_free(base_jgimp_dir);
		base_jgimp_dir = NULL;
	}
	if (jgimp_jarfile_location != NULL)
	{
		g_free(jgimp_jarfile_location);
		jgimp_jarfile_location = NULL;
	}

    /*  
     *  This next call tells the GIMP we've installed OK and are running
     *  It also let's us continue running (it sort of forks us, as far as I can tell)
     */
    gimp_extension_ack();

    if (is_plugin_server) {
        /*
         * Listen for calls to JGimp plug-ins
         */
        while (TRUE) 
        {
            gimp_extension_process (0); 
        }
    }
    else
    {
		/* Enter idle loop -- all communication from JGimp extensions are initiated by them */
		while (TRUE)
		{
#ifndef _WIN32
			sleep(500);
#else
			Sleep(1000);
#endif
		}
	}

}

