/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __JGIMP_JNI_UTILS_H__
#define __JGIMP_JNI_UTILS_H__

#include <jni.h>

/*
 * Starts the JVM, then starts up the JGimp proxy, loading any extensions
 * or plug-ins in the given directories.
 */
int jni_utils_load_jgimp(const char* jgimp_jarfile_location, const char* jgimp_base_dir, int loadPlugIns, int loadExtensions);

/*
 * Called when the application is shutting down to properly release
 * any resources held by JGimp and its extensions and plug-ins.
 */
void jni_utils_shutdown_jgimp();

/*
 * Called to ensure a thread is attached to the JVM. May not
 * be necessary, but it's cheap insurance
 */
JNIEnv* jni_utils_attach_thread();
/*
 * Detaches a thread when it's done using the JVM
 */
void    jni_utils_detach_thread();

/*
 * Constructs and throws an error when attempting to manipulate
 * an invalid drawable
 */
void    jni_utils_throw_invalid_drawable_exception(JNIEnv* env, int drawable_ID);
/*
 * Constructs and throws an error when attempting to manipulate
 * an invalid image
 */
void jni_utils_throw_invalid_image_exception(JNIEnv* env, int image_ID);
/*
 * Constructs and throws an error when an invalid pixel
 * region is requested
 */
void    jni_utils_throw_invalid_pixel_region_exception(JNIEnv* env, int x, int y, int desired_width, int desired_height, int drawable_width, int drawable_height);

/*
 * Constructs and throws an error when a buffer overflow
 * occurs when reading pixel data
 */
void    jni_utils_throw_buffer_overflow_exception(JNIEnv* env, int x, int y, int width, int height, int actual_width, int actual_height, int bytes_per_pixel, int num_bytes_in_array);

/*
 * Constructs and throws an error when a buffer underflow
 * occurs when writing pixel data
 */
void    jni_utils_throw_buffer_underflow_exception(JNIEnv* env, int x, int y, int width, int height, int actual_width, int actual_height, int bytes_per_pixel, int num_bytes_in_array);

void    jni_utils_throw_proxy_exception(JNIEnv *env, const char* error_msg);

void    jni_utils_throw_invalid_paint_tool_exception(JNIEnv *env, int tool_id);
#endif /* __JGIMP_JNI_UTILS_H__ */
