"""*** Incomplete script ***
from jgimp_core import *
import javax.swing as swing
import java.awt.dnd as dnd
import java.awt as awt
from java.awt.event import InputEvent

imageLists = []

window = swing.JFrame("JGimp Jython Batch Processor")
g_DesktopFrame = DesktopFrame()
window.contentPane.layout = awt.GridLayout(1, 1)
window.contentPane.add(desktopPane)

g_FileChooser = swing.JFileChooser()

class DesktopFrame(swing.JDesktopPane):
	def __init__(self):
		self.activeScript = None
		self.menuBar = self.getMenuBar()
		self.contentPane.add(self.menuBar)
		self.contentPane.componentResized = lambda evt: self.menuBar.size = (self.width, self.menuBar.height)
	def makeMenuItem(name, callback, hotkey=None, modifier=None):
		item = swing.JMenuItem(name)
		item.actionPerformed = lambda evt: callback()
		if (hotkey):
			item.accelerator = KeyStroke.getKeyStroke(hotkey, modifier)
		return item
	def getMenuBar(self):
		self.newScriptMenuItem     = makeMenuItem("New Script",          self.newScript,   "N", InputEvent.CTRLMASK)
		self.openScriptMenuItem    = makeMenuItem("Open Script...",      self.openScript,  "O", InputEvent.CTRLMASK)
		self.saveScriptMenuItem    = makeMenuItem("Save Script...",      self.saveScript,  "S", InputEvent.CTRLMASK)
		self.saveAsScriptMenuItem  = makeMenuItem("Save Script Copy...", self.saveScriptCopy)
		self.closeScriptMenuItem   = makeMenuItem("Close Script",        self.closeScript, "W", InputEvent.CTRLMASK)

		scriptsMenu = JMenu("Scripts")
		scriptsMenu.add(self.newScriptMenuItem)
		scriptsMenu.add(self.openScriptMenuItem)
		scriptsMenu.add(JMenuItem("_"))
		scriptsMenu.add(self.saveScriptMenuItem)
		scriptsMenu.add(self.saveAsScriptMenuItem)
		scriptsMenu.add(JMenuItem("_"))
		scriptsMenu.add(self.closeScriptMenuItem)

		self.openImageListMenuItem   = makeMenuItem("Open Image List...",      self.openImageList, "O", InputEvent.CTRLMASK + InputEvent.SHIFTMASK)
		self.saveImageListMenuItem   = makeMenuItem("Save Image List...",      self.saveImageList, "S", InputEvent.CTRLMASK + InputEvent.SHIFTMASK)
		self.saveAsImageListMenuItem = makeMenuItem("Save Image List Copy...", self.saveImageListCopy)
		self.clearImageListMenuItem  = makeMenuItem("Clear Image List",        self.clearImageList)

		imageListMenu = JMenu("Images")
		imageListMenu.add(self.openImageListMenuItem)
		imageListMenu.add(JMenu("_"))
		imageListMenu.add(self.saveImageListMenuItem)
		imageListMenu.add(self.saveAsImageListMenuItem)
		imageListMenu.add(JMenu("_"))
		imageListMenu.add(self.clearImageListMenuItem)

		self.runCurrentScriptMenuItem = makeMenuItem("Run Current Script",      self.runCurrentScript, "R", InputEvent.CTRLMASK)
		self.runAllScriptsMenuItem    = makeMenuItem("Run All Scripts",         self.runAllScripts,    "R", InputEvent.CTRLMASK + InputEvent.SHIFTMASK)
		self.cancelMenuItem           = makeMenuItem("Cancel Script Execution", self.cancelExecution,  "C", InputEvent.CTRLMASK)

		batchMenu = JMenu("Batch")
		batchMenu.add(self.runCurrentScriptMenuItem)
		batchMenu.add(self.runAllScriptsMenuItem)
		batchMenu.add(self.cancelMenuItem)

		menuBar = JMenuBar()
		menuBar.add(scriptsMenu)
		menuBar.add(imageListMenu)
		menuBar.add(batchMenu)
		return menuBar

	def scriptWindowActivatedEvt(self, evt):
		self.activeScript = evt.internalFrame
		updateMenus()
	def scriptWindowClosedEvt(self, evt):
		if (self.activeScript == evt.internalFrame):
			self.activeScript = None
			updateMenus()
	def updateMenus(self):
		pass
	def newScript(self, scriptFile=None):
		scriptWindow = ScriptView(scriptFile)
		scriptWindow.internalFrameActivated = self.scriptWindowActivatedEvt
		scriptWindow.internalFrameClosed = self.scriptWindowClosedEvt
		scriptWindow.visible = 1
	def openScript(self):
		result = g_FileChooser.showOpenDialog(None)
		if (result == swing.JFileChooser.APPROVE_OPTION):
			newScript(g_FileChooser.selectedFile)
	def saveScript(self):
		if (self.activeScript):
			self.activeScript.save()
	def saveScriptCopy(self):
		if (self.activeScript):
			self.activeScript.saveCopy()
	def closeScript(self):
		if (self.activeScript):
			self.activeScript.close()
	def newImageList(self, imageListFile=None):
		imageListWindow = ImageListView(imageListFile)
		imageListWindow.internalFrameActivated = self.imageListWindowActivatedEvt
		imageListWindow
	def openImageList(self):
		pass
	def saveImageList(self):
		pass
	def saveImageListCopy(self):
		pass
	def clearImageList(self):
		pass

class ScriptView(swing.JInternalFrame):
	def keyReleasedListener(self, evt):
		self.dirty = 1

	def __init__(self, scriptFile=None):
		swing.JInternalFrame.__init__(self, "Untitled", 1, 1, 1, 1)
		self.defaultCloseOperation = swing.WindowConstants.DISPOSE_ON_CLOSE
		self.internalFrameClosing = self.frameClosingEvt
		self.dirty = 0
		self.layout = awt.GridLayout(1, 1)
		self.textPane = swing.JTextPane()
		self.textPane.keyReleased = self.keyReleasedListener
		self.contentPane.add(swing.JScrollPane(self.textPane))
		if (scriptFile):
			self.scriptFile = scriptFile
			thisFile = open(scriptFile, "r")
			self.textPane.text = thisFile.read()
			thisFile.close()
			self.title = scriptFile.name
		self.size = (200, 200)

	def frameClosingEvt(self, evt):
		self.close()

		
	def setScriptText(self, script):
		self.textPane.text = script
	def getScriptText(self):
		return self.textPane.text
	def save(self):
		if (self.scriptFile):
			theFile = open(self.scriptFile, "w")
			theFile.write(self.textPane.text)
			theFile.close()
			self.dirty = 0
		else:
			result = g_FileChooser.showSaveDialog(None)
			if (result == swing.JFileChooser.APPROVE_OPTION):
				self.scriptFile = g_FileChooser.selectedFile
				self.saveScript()
	def saveCopy(self):
		result = g_FileChooser.showSaveDialog(None)
		if (result == swing.JFileChooser.APPROVE_OPTION):
			theFile = open(g_FileChooser.selectedFile, "w")
			theFile.write(self.textPane.text)
			theFile.close()
	def close(self):
		if (self.dirty):
			result = swing.JOptionPane.showConfirmDialog(None, "Save before closing?")
			if (result == swing.JOptionPane.CANCEL_OPTION):
				return
			elif (result == swing.JOptionPane.YES_OPTION):
				self.saveScript()
				if (self.dirty): # user cancelled out of there or an error, so don't quit
					return
		self.dirty = 0
		self.doDefaultCloseOperation()

class ImageListView(JInternalFrame):
	def __init__(self, imageListFile=None):
		pass
	def getImageList(self):
		pass
	def setImageList(self, imageList):
		pass
	def saveImageList(self):
		pass
	def sameImageListCopy(self):
		pass
	def clearImageList(self):
		pass



Desiderata...
Save as a plug-in and it brings up a dialog to scaffold
the process of defining all the info necessary to
install a plug-in (name, help, blurb, etc.). Saves
it in a format that can be read back in to be edited
again through same type of form.


"""
