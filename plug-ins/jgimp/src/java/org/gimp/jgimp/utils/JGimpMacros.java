/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.utils;

import java.awt.Dimension;

import org.gimp.jgimp.JGimpException;
import org.gimp.jgimp.JGimpImage;
import org.gimp.jgimp.JGimpLayer;

/**
 * A series of "macros," or common sequences of operations performed when
 * interacting with the PDB. This class is now deprecated, as its functionality has
 * been folded into the new object-oriented architecture.
 * @deprecated
 */
public class JGimpMacros
{
	public final static int DRAWABLE_TYPE_RGB          = 0;
	public final static int DRAWABLE_TYPE_RGBA         = 1;
	public final static int DRAWABLE_TYPE_GRAY         = 2;
	public final static int DRAWABLE_TYPE_GRAYA        = 3;
	public final static int DRAWABLE_TYPE_INDEXED      = 4;
	public final static int DRAWABLE_TYPE_INDEXEDA     = 5;
	public final static int DRAWABLE_TYPE_U16_RGB      = 6;
	public final static int DRAWABLE_TYPE_U16_RGBA     = 7;
	public final static int DRAWABLE_TYPE_U16_GRAY     = 8;
	public final static int DRAWABLE_TYPE_U16_GRAYA    = 9;
	public final static int DRAWABLE_TYPE_U16_INDEXED  = 10;
	public final static int DRAWABLE_TYPE_U16_INDEXEDA = 11;
	public final static int DRAWABLE_TYPE_FLOAT_RGB    = 12;
	public final static int DRAWABLE_TYPE_FLOAT_RGBA   = 13;
	public final static int DRAWABLE_TYPE_FLOAT_GRAY   = 14;
	public final static int DRAWABLE_TYPE_FLOAT_GRAYA  = 15;

	/**
	 * Makes a "thumbnail image" by duplicating the given image and reducing
	 * it to fit within the maximum width and height given. This method is useful
	 * for generating previews for plug-in dialog boxes. 
	 */
    public synchronized static JGimpImage createThumbnail(JGimpImage inImage, int inMaxWidth, int inMaxHeight) throws JGimpException
	{
		float percentScale = 0.0F;
		float horizontalDif = 0.0F;
		float verticalDif = 0.0F;
		int newWidth;
		int newHeight;
		JGimpLayer activeLayer = inImage.getActiveLayer();
		Dimension layerSize = activeLayer.getSize();

		verticalDif = ((float)inMaxHeight) / layerSize.height;
		horizontalDif = ((float)inMaxWidth) / layerSize.width;
		percentScale = Math.min(verticalDif, horizontalDif);
		if (percentScale > 1.0)
		{
			newWidth = layerSize.width;
			newHeight = layerSize.height;
		}
		else
		{
			newWidth = (int) (layerSize.width * percentScale);
			newHeight = (int) (layerSize.height * percentScale);
		}

		JGimpImage thumbnailImage = inImage.duplicate();
        thumbnailImage.scale(newWidth, newHeight);
		return thumbnailImage;
	}

}
