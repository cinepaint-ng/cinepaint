/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp;

import org.gimp.jgimp.nativewrappers.JGimpColor;
import org.gimp.jgimp.nativewrappers.JGimpEnd;
import org.gimp.jgimp.nativewrappers.JGimpFloat;
import org.gimp.jgimp.nativewrappers.JGimpFloatArray;
import org.gimp.jgimp.nativewrappers.JGimpInt16;
import org.gimp.jgimp.nativewrappers.JGimpInt16Array;
import org.gimp.jgimp.nativewrappers.JGimpInt32;
import org.gimp.jgimp.nativewrappers.JGimpInt32Array;
import org.gimp.jgimp.nativewrappers.JGimpInt8;
import org.gimp.jgimp.nativewrappers.JGimpInt8Array;
import org.gimp.jgimp.nativewrappers.JGimpPDBStatus;
import org.gimp.jgimp.nativewrappers.JGimpParasite;
import org.gimp.jgimp.nativewrappers.JGimpString;
import org.gimp.jgimp.nativewrappers.JGimpStringArray;

/**
 * Exception thrown when a procedure call is made using a GimpApp object,
 * and the data passed for a parameter cannot be coerced or transformed
 * into the needed form.
 * @see org.gimp.jgimp.GimpApp#callProcedure(JGimpProcedureDescriptor, Object[])
 */
public class JGimpInvalidDataCoercionException extends JGimpException {
    
    public JGimpInvalidDataCoercionException(Class desiredType, Object data) {
        super("Invalid data cohercion. Desired type: " + desiredType + " data: " + data.toString() + " type: " + data.getClass());
    }

    public JGimpInvalidDataCoercionException(int desiredType, Object data) {
        this(getDesiredTypeClass(desiredType), data);
    }
    private static Class getDesiredTypeClass(int desiredType) {
        switch (desiredType) {
            case JGimpPDBArgTypeConstants.PDB_BOUNDARY:
                return JGimpBoundary.class;
                
            case JGimpPDBArgTypeConstants.PDB_CHANNEL:
                return JGimpChannel.class;
                
            case JGimpPDBArgTypeConstants.PDB_COLOR:
                return JGimpColor.class;
                
            case JGimpPDBArgTypeConstants.PDB_DISPLAY:
                return JGimpDisplay.class;
                
            case JGimpPDBArgTypeConstants.PDB_DRAWABLE:
                return JGimpDrawable.class;
                
            case JGimpPDBArgTypeConstants.PDB_END:
                return JGimpEnd.class;
                
            case JGimpPDBArgTypeConstants.PDB_FLOAT:
                return JGimpFloat.class;
                
            case JGimpPDBArgTypeConstants.PDB_FLOATARRAY:
                return JGimpFloatArray.class;
                
            case JGimpPDBArgTypeConstants.PDB_IMAGE:
                return JGimpImage.class;
                
            case JGimpPDBArgTypeConstants.PDB_INT16:
                return JGimpInt16.class;
                
            case JGimpPDBArgTypeConstants.PDB_INT16ARRAY:
                return JGimpInt16Array.class;
                
            case JGimpPDBArgTypeConstants.PDB_INT32:
                return JGimpInt32.class;
                
            case JGimpPDBArgTypeConstants.PDB_INT32ARRAY:
                return JGimpInt32Array.class;
                
            case JGimpPDBArgTypeConstants.PDB_INT8:
                return JGimpInt8.class;
                
            case JGimpPDBArgTypeConstants.PDB_INT8ARRAY:
                return JGimpInt8Array.class;
                
            case JGimpPDBArgTypeConstants.PDB_LAYER:
                return JGimpLayer.class;
                
            case JGimpPDBArgTypeConstants.PDB_PARASITE:
                return JGimpParasite.class;
                
            case JGimpPDBArgTypeConstants.PDB_PATH:
                return JGimpPath.class;
                
            case JGimpPDBArgTypeConstants.PDB_REGION:
                return JGimpRegion.class;
                
            case JGimpPDBArgTypeConstants.PDB_SELECTION:
                return JGimpSelection.class;
                
            case JGimpPDBArgTypeConstants.PDB_STATUS:
                return JGimpPDBStatus.class;
                
            case JGimpPDBArgTypeConstants.PDB_STRING:
                return JGimpString.class;
                
            case JGimpPDBArgTypeConstants.PDB_STRINGARRAY:
                return JGimpStringArray.class;
                
            default:
                return null;
        }
    }
    
}
