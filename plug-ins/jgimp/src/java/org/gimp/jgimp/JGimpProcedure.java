/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp;

import org.gimp.jgimp.plugin.JGimpParamDescriptor;
import org.gimp.jgimp.plugin.JGimpProcedureDescriptor;

/**
 * An object representing a GIMP function. Essentially,
 * an instantiation of a GoF Command pattern. This class
 * is not really necessary for anything, and may be removed
 * if no one uses it.
 */
public class JGimpProcedure {

    private GimpApp m_App = null;
    private JGimpProcedureDescriptor m_ProcedureDescriptor = null;
    private int[] m_ArgTypes = null;
    private JGimpData[] m_Args = null;

    JGimpProcedure(GimpApp app, JGimpProcedureDescriptor procedureDescription) {
        m_App = app;
        m_ProcedureDescriptor = procedureDescription;

        JGimpParamDescriptor[] paramDescriptions = m_ProcedureDescriptor.getParameterDescriptions();
        m_ArgTypes = new int[paramDescriptions.length];
        m_Args = new JGimpData[paramDescriptions.length];
        for (int i = 0; i < paramDescriptions.length; i++) {
            m_ArgTypes[i] = paramDescriptions[i].getGimpPDBArgType();
        }
    }

    /**
     * Sets the specified data element in the list to a byte (Int8)
     */
    public void setArgument(int index, byte b) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], new Byte(b));
    }
    /**
     * Sets the specified data element in the list to a short (Int16)
     */
    public void setArgument(int index, short s) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], new Short(s));
    }
    /**
     * Sets the specified data element in the list to an int (Int32)
     */
    public void setArgument(int index, int i) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], new Integer(i));
    }
    /**
     * Sets the specified data element in the list to a double (what the GIMP considers a "float")
     */
    public void setArgument(int index, double d) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], new Double(d));
    }
    /**
     * Sets the specified data element in the list to a string
     */
    public void setArgument(int index, String s) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], s);
    }
    /**
     * Sets the specified data element in the list to a byte array (Int8 array)
     */
    public void setArgument(int index, byte[] array) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], array);
    }
    /**
     * Sets the specified data element in the list to a short array (Int16 array)
     */
    public void setArgument(int index, short[] array) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], array);
    }
    /**
     * Sets the specified data element in the list to an int array (Int32 array)
     */
    public void setArgument(int index, int[] array) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], array);
    }
    /**
     * Sets the specified data element in the list to a double array (what the GIMP calls a "float" array)
     */
    public void setArgument(int index, double[] array) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], array);
    }
    /**
     * Sets the specified data element in the list to a string array
     */
    public void setArgument(int index, String[] array) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], array);
    }
    /**
     * Sets the specified data element in the list to the given data object
     */
    public void setArgument(int index, JGimpData inData) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], inData);
    }
    /**
     * Sets the specified data element in the list to the given data object
     */
    public void setArgument(int index, Object object) throws JGimpInvalidDataCoercionException {
        m_Args[index] = m_App.convertJavaObjectToJGimpData(m_ArgTypes[index], object);
    }
    /**
     * Returns a copy of the data element at the given index
     */
    public JGimpData getArgument(int index) throws JGimpInvalidDataCoercionException {
        return (JGimpData) m_Args[index];
    }

    /**
     * Executes the procedure this object represents, using the data
     * previously set
     * @return The results of executing the procedure
     * @throws JGimpException
     */
    public JGimpData[] execute() throws JGimpException {
        return m_App.callProcedure(m_ProcedureDescriptor.getName(), m_Args);
    }
}
