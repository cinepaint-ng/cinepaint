/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.plugin;

import org.gimp.jgimp.JGimpData;
import org.gimp.jgimp.nativewrappers.JGimpPDBStatus;

/**
 * Used by a plug-in to return information back to the GIMP after
 * it is finished executing. This class enforces the need to send
 * back a status object to the GIMP.
 */
public class JGimpPlugInReturnValues {
    private JGimpPDBStatus m_Status = null;
    private JGimpData[] m_ReturnData = null;

    /**
     * Constructs a new return value with the given status and return data.
     * @param status The status of executing a plug-in.
     * @param returnData The return data (without the status data)
     */
    public JGimpPlugInReturnValues(JGimpPDBStatus status, JGimpData[] returnData) {
        m_Status = status;
        if (returnData == null) {
            m_ReturnData = new JGimpData[0];
        } else {
            m_ReturnData = returnData;
        }
    }
    public JGimpPDBStatus getReturnStatus() {
        return m_Status;
    }
    public JGimpData[] getReturnData() {
        return m_ReturnData;
    }
    /**
     * Convenience function to create a return data object indicating an
     * execution error was encountered.
     */
    public static JGimpPlugInReturnValues returnExecutionError() {
        return new JGimpPlugInReturnValues(JGimpPDBStatus.getExecutionErrorStatus(), null);
    }
    /**
     * Convenience function to create a return data object indicating a
     * calling error was encountered.
     */
    public static JGimpPlugInReturnValues returnCallingError() {
        return new JGimpPlugInReturnValues(JGimpPDBStatus.getCallingErrorStatus(), null);
    }
    /**
     * Convenience function to create an object indicating pass-through.
     */
    public static JGimpPlugInReturnValues returnPassThrough() {
        //TODO: figure out what "pass-through" means
        return new JGimpPlugInReturnValues(JGimpPDBStatus.getPassThroughStatus(), null);
    }
    /**
     * Convenience function to create an object indicating pass-through.
     */
    public static JGimpPlugInReturnValues returnPassThrough(JGimpData[] inReturnData) {
        return new JGimpPlugInReturnValues(JGimpPDBStatus.getPassThroughStatus(), inReturnData);
    }

    /**
     * Convenience function to create an object indicating the plug-in executed successfully, but has
     * no data to return.
     */
    public static JGimpPlugInReturnValues returnSuccess() {
        return new JGimpPlugInReturnValues(JGimpPDBStatus.getSuccessStatus(), null);
    }
    /**
     * Convenience function to create an object indicating the plug-in executed successfully,
     * and is returning the following data (do not include the status object).
     */
    public static JGimpPlugInReturnValues returnSuccess(JGimpData[] inReturnData) {
        return new JGimpPlugInReturnValues(JGimpPDBStatus.getSuccessStatus(), inReturnData);
    }

    /**
     * Convenience function to create an object indicating the plug-in was cancelled by the user.
     */
    public static JGimpPlugInReturnValues returnCancel() {
        return new JGimpPlugInReturnValues(JGimpPDBStatus.getCancelStatus(), null);
    }

}
