/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp;

/**
* 	  Integer constants corresponding to the GIMP's
* 	  different data types for PDB arguments. These
*     different data types are actually realized as classes
*     within JGimp. For example, a PDB_IMAGE type translates
*     to a JGimpImage.
*/
public class JGimpPDBArgTypeConstants
{
	public final static int PDB_INT32         = 0;
	public final static int PDB_INT16         = 1;
	public final static int PDB_INT8          = 2;
	public final static int PDB_FLOAT         = 3;
	public final static int PDB_STRING        = 4;
	public final static int PDB_INT32ARRAY    = 5;
	public final static int PDB_INT16ARRAY    = 6;
	public final static int PDB_INT8ARRAY     = 7;
	public final static int PDB_FLOATARRAY    = 8;
	public final static int PDB_STRINGARRAY   = 9;
	public final static int PDB_COLOR         = 10;
	public final static int PDB_REGION        = 11;
	public final static int PDB_DISPLAY       = 12;
	public final static int PDB_IMAGE         = 13;
	public final static int PDB_LAYER         = 14;
	public final static int PDB_CHANNEL       = 15;
	public final static int PDB_DRAWABLE      = 16;
	public final static int PDB_SELECTION     = 17;
	public final static int PDB_BOUNDARY      = 18;
	public final static int PDB_PATH          = 19;
	public final static int PDB_PARASITE      = 20;
	public final static int PDB_STATUS        = 21;
	public final static int PDB_END           = 22;
    
    public static boolean isArrayType(int type) {
        return (type == JGimpPDBArgTypeConstants.PDB_FLOATARRAY) ||
                   (type == JGimpPDBArgTypeConstants.PDB_INT16ARRAY) ||
                   (type == JGimpPDBArgTypeConstants.PDB_INT32ARRAY) ||
                   (type == JGimpPDBArgTypeConstants.PDB_INT8ARRAY) ||
                   (type == JGimpPDBArgTypeConstants.PDB_STRINGARRAY);
    }
}
