/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.paint;

public class JGimpPaintToolFactory
{
	private static final int PENCIL_ID            = 18;
	private static final int PAINTBRUSH_ID        = 19;
	private static final int ERASER_ID            = 20;
	private static final int AIRBRUSH_ID          = 21;
	private static final int CLONE_ID             = 22;
	private static final int CONVOLVE_ID          = 23;
	private static final int SMUDGE_ID            = 24;
	private static final int DODGEBURN_ID         = 25;
	private static final int CURRENT_GIMP_TOOL_ID = -100;

	private static JGimpPaintTool s_PencilTool       = null;
	private static JGimpPaintTool s_PaintBrushTool   = null;
	private static JGimpPaintTool s_EraserTool       = null;
	private static JGimpPaintTool s_AirBrushTool     = null;
	private static JGimpPaintTool s_CloneTool        = null;
	private static JGimpPaintTool s_ConvolveTool     = null;
	private static JGimpPaintTool s_SmudgeTool       = null;
	private static JGimpPaintTool s_DodgeBurnTool    = null;
	private static JGimpPaintTool s_CurrentGIMPTool  = null;

	public static JGimpPaintTool getPencilTool()
	{
		if (s_PencilTool == null) {
			s_PencilTool = new JGimpPaintTool() {
				public int getToolID() {
					return PENCIL_ID;
				}
			};
		}
		return s_PencilTool;
	}
	public static JGimpPaintTool getPaintBrushTool()
	{
		if (s_PaintBrushTool == null) {
			s_PaintBrushTool = new JGimpPaintTool() {
				public int getToolID() {
					return PAINTBRUSH_ID;
				}
			};
		}
		return s_PaintBrushTool;
	}
	public static JGimpPaintTool getEraserTool()
	{
		if (s_EraserTool == null) {
			s_EraserTool = new JGimpPaintTool() {
				public int getToolID() {
					return ERASER_ID;
				}
			};
		}
		return s_EraserTool;
	}
	public static JGimpPaintTool getAirBrushTool()
	{
		if (s_AirBrushTool == null) {
			s_AirBrushTool = new JGimpPaintTool() {
				public int getToolID() {
					return AIRBRUSH_ID;
				}
			};
		}
		return s_AirBrushTool;
	}
	public static JGimpPaintTool getCloneTool()
	{
		if (s_CloneTool == null) {
			s_CloneTool = new JGimpPaintTool() {
				public int getToolID() {
					return CLONE_ID;
				}
			};
		}
		return s_CloneTool;
	}
	public static JGimpPaintTool getConvolveTool()
	{
		if (s_ConvolveTool == null) {
			s_ConvolveTool = new JGimpPaintTool() {
				public int getToolID() {
					return CONVOLVE_ID;
				}
			};
		}
		return s_ConvolveTool;
	}
	public static JGimpPaintTool getSmudgeTool()
	{
		if (s_SmudgeTool == null) {
			s_SmudgeTool = new JGimpPaintTool() {
				public int getToolID() {
					return SMUDGE_ID;
				}
			};
		}
		return s_SmudgeTool;
	}
	public static JGimpPaintTool getDodgeBurnTool()
	{
		if (s_DodgeBurnTool == null) {
			s_DodgeBurnTool = new JGimpPaintTool() {
				public int getToolID() {
					return DODGEBURN_ID;
				}
			};
		}
		return s_DodgeBurnTool;
	}
	/**
	 * This is a kludge until we completely take over
	 * the GIMP. However, for now, let the user choose
	 * the current paint tool using the GIMP's palette.
	 */
	public static JGimpPaintTool useCurrentGIMPTool()
	{
		if (s_CurrentGIMPTool == null) {
			s_CurrentGIMPTool = new JGimpPaintTool() {
				public int getToolID() {
					return CURRENT_GIMP_TOOL_ID;
				}
			};
		}
		return s_CurrentGIMPTool;
	}
}
