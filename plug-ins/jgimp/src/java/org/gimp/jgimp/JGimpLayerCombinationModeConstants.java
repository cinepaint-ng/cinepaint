/*
 * Created on Apr 16, 2003 by Michael Terry
 *
 */
package org.gimp.jgimp;

/**
 * Constants defining the different types of layer combination modes.
 */
public class JGimpLayerCombinationModeConstants {
    public final static int NORMAL_MODE = 0;
    public final static int DISSOLVE_MODE = 1;
    public final static int BEHIND_MODE = 2;
    public final static int MULTIPLY_MODE = 3;
    public final static int SCREEN_MODE = 4;
    public final static int OVERLAY_MODE = 5;
    public final static int DIFFERENCE_MODE = 6;
    public final static int ADDITION_MODE = 7;
    public final static int SUBTRACT_MODE = 8;
    public final static int DARKEN_ONLY_MODE = 9;
    public final static int LIGHTEN_ONLY_MODE = 10;
    public final static int HUE_MODE = 11;
    public final static int SATURATION_MODE = 12;
    public final static int COLOR_MODE = 13;
    public final static int VALUE_MODE = 14;
    public final static int DIVIDE_MODE = 15;
}
