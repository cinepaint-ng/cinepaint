/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.nativewrappers;

import org.gimp.jgimp.*;

/**
 * Encapsulates data about a status object in the GIMP.
 */
public class JGimpPDBStatus extends JGimpData {
    private int m_Status = -1;

    public interface Type {
        public static final int GIMP_PDB_EXECUTION_ERROR = 0;
        public static final int GIMP_PDB_CALLING_ERROR = 1;
        public static final int GIMP_PDB_PASS_THROUGH = 2;
        public static final int GIMP_PDB_SUCCESS = 3;
        public static final int GIMP_PDB_CANCEL = 4;
    }
	private static JGimpPDBStatus s_ExecutionErrorStatus = null;
	private static JGimpPDBStatus s_CallingErrorStatus = null;
	private static JGimpPDBStatus s_PassThroughStatus = null;
	private static JGimpPDBStatus s_SuccessStatus = null;
	private static JGimpPDBStatus s_CancelStatus = null;

    public String toString() {
        String returnString = "JGimpPDBStatus, status: ";
        switch (m_Status) {
            case Type.GIMP_PDB_EXECUTION_ERROR :
                return returnString + "PDB execution error";
            case Type.GIMP_PDB_CALLING_ERROR :
                return returnString + "PDB calling error";
            case Type.GIMP_PDB_PASS_THROUGH :
                return returnString + "PDB pass through";
            case Type.GIMP_PDB_SUCCESS :
                return returnString + "PDB success";
            case Type.GIMP_PDB_CANCEL :
                return returnString + "PDB cancel";
            default :
                return returnString + "ERROR: Unknown PDB status in JGimpPDBStatus. Shouldn't happen";
        }
    }

    public JGimpPDBStatus(int inStatus) {
        m_Status = inStatus;
    }
    public static JGimpPDBStatus getExecutionErrorStatus() {
		if (s_ExecutionErrorStatus == null) {
			s_ExecutionErrorStatus = new JGimpPDBStatus(Type.GIMP_PDB_EXECUTION_ERROR);
		}
		return s_ExecutionErrorStatus;
    }
    public static JGimpPDBStatus getCallingErrorStatus() {
		if (s_CallingErrorStatus == null) {
        	s_CallingErrorStatus = new JGimpPDBStatus(Type.GIMP_PDB_CALLING_ERROR);
		}
		return s_CallingErrorStatus;
    }
    public static JGimpPDBStatus getPassThroughStatus() {
		if (s_PassThroughStatus == null) {
        	s_PassThroughStatus = new JGimpPDBStatus(Type.GIMP_PDB_PASS_THROUGH);
		}
		return s_PassThroughStatus;
    }
    public static JGimpPDBStatus getSuccessStatus() {
		if (s_SuccessStatus == null) {
			s_SuccessStatus = new JGimpPDBStatus(Type.GIMP_PDB_SUCCESS);
		}
		return s_SuccessStatus;
    }
    public static JGimpPDBStatus getCancelStatus() {
        if (s_CancelStatus == null) {
			s_CancelStatus = new JGimpPDBStatus(Type.GIMP_PDB_CANCEL);
		}
		return s_CancelStatus;
    }

    public boolean equals(Object inRight) {
        if (inRight == null) {
            return false;
        }

        if (inRight instanceof JGimpPDBStatus) {
            return (((JGimpPDBStatus) inRight).getStatus() == this.m_Status);
        }
        return false;
    }

    public int getStatus() {
        return m_Status;
    }

    /**
     * @see org.gimp.jgimp.JGimpData#convertToInt()
     */
    public int convertToInt() throws JGimpInvalidDataCoercionException {
        return m_Status;
    }

    /**
     * @see org.gimp.jgimp.JGimpData#getGimpPDBArgType()
     */
    public int getGimpPDBArgType() {
        return JGimpPDBArgTypeConstants.PDB_STATUS;
    }
}
