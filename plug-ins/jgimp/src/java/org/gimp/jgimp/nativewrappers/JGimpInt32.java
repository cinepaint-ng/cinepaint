/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.nativewrappers;

import org.gimp.jgimp.*;

/**
 * Encapsulates an Int32, or 32-bit integer (a regular Java int).
 */
public class JGimpInt32 extends JGimpData {
    private int m_Value = 0;
    public JGimpInt32(int inValue) {
        m_Value = inValue;
    }
    public String toString() {
        return "JGimpInt32, value: " + m_Value;
    }

    public boolean equals(Object inRight) {
        if (inRight == null) {
            return false;
        }

        if (inRight instanceof JGimpInt32) {
            return (((JGimpInt32) inRight).getValue() == this.m_Value);
        }
        return false;
    }
    
    public int getValue() {
        return m_Value;
    }
    /**
     * @see org.gimp.jgimp.JGimpData#convertToInt()
     */
    public int convertToInt() throws JGimpInvalidDataCoercionException {
        return m_Value;
    }
    
    /**
     * @see org.gimp.jgimp.JGimpData#convertToString()
     */
    public String convertToString() throws JGimpInvalidDataCoercionException {
        return Integer.toString(m_Value);
    }

    /**
     * @see org.gimp.jgimp.JGimpData#getGimpPDBArgType()
     */
    public int getGimpPDBArgType() {
        return JGimpPDBArgTypeConstants.PDB_INT32;
    }

}
