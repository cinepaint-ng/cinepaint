/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.nativewrappers;

import org.gimp.jgimp.*;

/**
 * Encapsulates an 8-bit int (a byte in Java).
 */
public class JGimpInt8 extends JGimpData {
    private byte m_Value = 0;

    public JGimpInt8(byte inValue) {
        m_Value = inValue;
    }

    public String toString() {
        return "JGimpInt8, value: " + m_Value;
    }

    public boolean equals(Object inRight) {
        if (inRight == null) {
            return false;
        }

        if (inRight instanceof JGimpInt8) {
            return (((JGimpInt8) inRight).getValue() == this.m_Value);
        }
        return false;
    }

    /**
     * @see org.gimp.jgimp.JGimpData#convertToByte()
     */
    public byte convertToByte() throws JGimpInvalidDataCoercionException {
        return m_Value;
    }
    
    /**
     * @see org.gimp.jgimp.JGimpData#convertToString()
     */
    public String convertToString() throws JGimpInvalidDataCoercionException {
        return Byte.toString(m_Value);
    }


    public byte getValue() {
        return m_Value;
    }

    /**
     * @see org.gimp.jgimp.JGimpData#getGimpPDBArgType()
     */
    public int getGimpPDBArgType() {
        return JGimpPDBArgTypeConstants.PDB_INT8;
    }

}
