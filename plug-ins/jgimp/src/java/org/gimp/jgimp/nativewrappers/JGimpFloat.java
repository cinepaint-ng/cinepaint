/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.nativewrappers;

import org.gimp.jgimp.*;

/**
 * Encapsulates a single float value (represented as a double
 * in Java).
 */
public class JGimpFloat extends JGimpData {
    /* Note: GIMP says "float" but it's really a double... */
    private double m_Value = 0.0F;

    public JGimpFloat(double inValue) {
        m_Value = inValue;
    }

    public String toString() {
        return "JGimpFloat, value: " + m_Value;
    }

    public boolean equals(Object inRight) {
        if (inRight == null) {
            return false;
        }

        if (inRight instanceof JGimpFloat) {
            return (((JGimpFloat) inRight).getValue() == this.m_Value);
        }
        return false;
    }

    /**
     * @see org.gimp.jgimp.JGimpData#convertToDouble()
     */
    public double convertToDouble() throws JGimpInvalidDataCoercionException {
        return m_Value;
    }
    
    public double getValue() {
        return m_Value;
    }
    
    /**
     * @see org.gimp.jgimp.JGimpData#convertToString()
     */
    public String convertToString() throws JGimpInvalidDataCoercionException {
        return Double.toString(m_Value);
    }

    /**
     * @see org.gimp.jgimp.JGimpData#getGimpPDBArgType()
     */
    public int getGimpPDBArgType() {
        return JGimpPDBArgTypeConstants.PDB_FLOAT;
    }

}
