/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp.nativewrappers;

import org.gimp.jgimp.*;

/**
 * Encapsulates an array of strings.
 */
public class JGimpStringArray extends JGimpData {
    private String[] m_Data = null;

    public JGimpStringArray(String[] inArray) {
        if ((inArray == null) || (inArray.length < 1)) {
            m_Data = new String[1];
            m_Data[0] = "";
        } else {
            m_Data = new String[inArray.length];
            System.arraycopy(inArray, 0, m_Data, 0, inArray.length);
        }
    }

    public String toString() {
        return "JGimpStringArray";
    }

    public String[] getArray() {
        return m_Data;
    }

    /**
     * @see org.gimp.jgimp.JGimpData#convertToStringArray()
     */
    public String[] convertToStringArray() throws JGimpInvalidDataCoercionException {
        return m_Data;
    }

    public boolean equals(Object inRight) {
        if (inRight == null) {
            return false;
        }

        if (inRight instanceof JGimpStringArray) {
            String[] rightArray = ((JGimpStringArray) inRight).getArray();
            if (rightArray.length != this.m_Data.length) {
                return false;
            }
            for (int i = 0; i < rightArray.length; i++) {
                if (!this.m_Data[i].equals(rightArray[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    /**
     * @see org.gimp.jgimp.JGimpData#getGimpPDBArgType()
     */
    public int getGimpPDBArgType() {
        return JGimpPDBArgTypeConstants.PDB_STRINGARRAY;
    }

}
