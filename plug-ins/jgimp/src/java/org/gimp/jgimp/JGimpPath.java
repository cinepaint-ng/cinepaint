/* 
 * JGimp - A Java extension for the GIMP
 * Copyright (C) 2002  Georgia Tech Research Corp.
 * Written by Michael Terry, mterry@cc.gatech.edu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.gimp.jgimp;



/**
 * Encapsulates data identifying a path in the GIMP.
 */
public class JGimpPath extends JGimpData {
    private int m_ID = -1;
    public JGimpPath(int inID) {
        m_ID = inID;
    }
    public String toString() {
        return "JGimpPath, value: " + m_ID;
    }
    public int getID() {
        return m_ID;
    }
    public boolean equals(Object inRight) {
        if (inRight == null) {
            return false;
        }

        if (inRight instanceof JGimpPath) {
            return (((JGimpPath) inRight).getID() == this.getID());
        }
        return false;
    }
    /**
     * @see org.gimp.jgimp.JGimpData#convertToInt()
     */
    public int convertToInt() throws JGimpInvalidDataCoercionException {
        return m_ID;
    }

    /**
     * @see org.gimp.jgimp.JGimpData#getGimpPDBArgType()
     */
    public int getGimpPDBArgType() {
        return JGimpPDBArgTypeConstants.PDB_PATH;
    }

}
