/* $RCSfile$
 * 
 * $Author$
 * $Date$
 * $Revision$
 * 
 * $Log$
 * Revision 1.1  2004/02/10 01:05:36  robinrowe
 * add 0.18-1
 *
 */

#ifndef READPIC_H
#define READPIC_H

#include "gtk/gtk.h"

guint32 readPicImage(char *fileName, gint32 *imageID);

#endif // READPIC_H
