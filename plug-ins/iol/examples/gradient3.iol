
# A gradient example
#
# $Id$

size a b ;

alpha = ( 1.0 ) ;

v = 1.0 / b ;

initend ;

pos x y;

y *= v ;
y !-= 1.0 ;

output y y y alpha;

end ;

