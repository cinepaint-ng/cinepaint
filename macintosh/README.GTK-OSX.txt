Description: [README.GTK-OSX.txt] GTK1/GTK2 Mac OS X Notes
Copyright 2008/03/11 Robin Rowe <robin.rowe@cinepaint.org>
License: BSD <http://opensource.org/licenses/bsd-license.php>

GTK+OSX is a GTK1 Mac OS X port. It's a separate effort from 
the port of GTK2 to Mac OS X. Both ports are in an incomplete
state. CinePaint can be built with either, but will have 
issues.

GTK+OSX is a SourceForge project:

http://sourceforge.net/projects/gtk-osx/
cvs -d:pserver:anonymous@gtk-osx.cvs.sourceforge.net:/cvsroot/gtk-osx login
cvs -z3 -d:pserver:anonymous@gtk-osx.cvs.sourceforge.net:/cvsroot/gtk-osx co -P modulename

GTK2 Mac OS X is part of the GTK project:

www.gtk.org

###