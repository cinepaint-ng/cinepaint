/* shm/app_shm.h
// Moved existing shared memory app code into separate file
// Created by Robin.Rowe@MovieEditor.com on March 9, 2003
// Refer to app_shm.c for (GPL) license */

#ifndef APP_SHM_H
#define APP_SHM_H

gint32  gimp_shmem_init (int p, key_t k, int s, long o, int xs, int ys);
static void       init_shmem (void);

#endif