mv libgimp lib

mv gimptips.txt tips.txt

mkdir wire
mkdir obsolete

mv app/shmbuf.c obsolete/shmbuf.c
mv app/shmbuf.h obsolete/shmbuf.h
mv app/regex.c obsolete/regex.c
mv app/regex.h obsolete/regex.h

mv lib/gimpenums.h wire
mv lib/gimpfeatures.h wire
mv lib/gimpprotocol.h wire
mv lib/gimpprotocol.c wire
mv lib/gimpwire.c wire
mv lib/gimpwire.h wire

mv filmgimp.1 cinepaint.1
mv filmgimp.m4 cinepaint.m4
mv filmgimp.spec cinepaint.spec
mv filmgimp.spec.in cinepaint.spec.in
mv filmgimptool.1 filmgimptool.1 cptool.1
mv filmgimptool.in cptool.in
mv gimp_logo.ppm logo.ppm
mv gimp_splash.ppm splash.ppm
mv gimprc rc
mv gimprc.in rc.in
mv gimprc_user rc_user
mv gimprc_user.in rc_user.in

mv app/gimpbrush.c app/brush.c
mv app/gimpbrush.h app/brush.h
mv app/gimpbrushgenerated.c app/brushgenerated.c
mv app/gimpbrushgenerated.h app/brushgenerated.h
mv app/gimpbrushlist.c app/brushlist.c
mv app/gimpbrushlist.h app/brushlist.h
mv app/gimpbrushlistF.h app/brushlistF.h
mv app/gimpbrushlistP.h app/brushlistP.h
mv app/gimpfeatures.h app/features.h
mv app/gimplist.c app/list.c
mv app/gimplist.h app/list.h
mv app/gimplistF.h app/listF.h
mv app/gimplistP.h app/listP.h
mv app/gimpobject.c app/object.c
mv app/gimpobject.h app/object.h
mv app/gimpobjectF.h app/objectF.h
mv app/gimpobjectP.h app/objectP.h
mv app/gimprc.c app/rc.c
mv app/gimprc.h app/rc.h
mv app/gimpsignal.c app/signal_type.c
mv app/gimpsignal.h app/signal_type.h

mv lib/gimp.h lib/plugin_main.h
mv lib/gimp.c lib/plugin_main.c
mv lib/gimp_pdb.h  lib/pdb.h
mv lib/gimpchainbutton.c  lib/chainbutton.c
mv lib/gimpchainbutton.h lib/chainbutton.h
mv lib/gimpchannel.c lib/channel.c
mv lib/gimpdialog.c lib/gdialog.c
mv lib/gimpdialog.h  lib/gdialog.h
mv lib/gimpdisplay.c lib/gdisplay.c
mv lib/gimpdrawable.c  lib/gdrawable.c
mv lib/gimpfeatures.h.in lib/version.h.in
mv lib/gimpfloat16.h lib/float16.h
mv lib/gimpgradient.c lib/gradient.c
mv lib/gimphelpui.c lib/helpui.c
mv lib/gimphelpui.h lib/helpui.h
mv lib/gimpimage.c lib/gimage.c
mv lib/gimplayer.c lib/player.c
mv lib/gimplimits.h lib/glimits.h
mv lib/gimpmath.h lib/gmath.h
mv lib/gimpmenu.c lib/gmenu.c
mv lib/gimpmenu.h lib/gmenu.h
mv lib/gimppalette.c lib/gpalette.c
mv lib/gimppixelrgn.c lib/pixelrgn.c
mv lib/gimppixmap.c lib/gpixmap.c
mv lib/gimppixmap.h lib/gpixmap.h
mv lib/gimpplugin_pdb.c lib/plugin_pdb.c
mv lib/gimpplugin_pdb.h lib/plugin_pdb.h
mv lib/gimpsizeentry.c lib/size_entry.c
mv lib/gimpsizeentry.h lib/size_entry.h
mv lib/gimptile.c lib/tile.c
mv lib/gimpui.h lib/ui.h
mv lib/gimpunit.c lib/unit.c
mv lib/gimpunit.h lib/unit.h
mv lib/gimpunit_pdb.c lib/unit_pdb.c
mv lib/gimpunit_pdb.h lib/unit_pdb.h
mv lib/gimpunitmenu.c lib/unitmenu.c
mv lib/gimpunitmenu.h lib/unitmenu.h
mv lib/gimpwidgets.c  lib/widgets.c
mv lib/gimpwidgets.h lib/widgets.h
mv lib/libgimp-intl.h lib/intl.h

mv wire/gimpenums.h wire/enums.h
mv wire/gimpprotocol.c wire/protocol.c
mv wire/gimpprotocol.h wire/protocol.h
mv wire/gimpwire.c wire/wire.c
mv wire/gimpwire.h wire/wire.h






