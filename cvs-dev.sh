echo cvs-dev.sh Robin.Rowe@CinePaint.org
echo License BSD 10.18.2008
echo INSTRUCTIONS: Replace my name with your SF login below...
echo If you are not a member of the CinePaint team, you want cvs-read.sh

export CVS_RSH=ssh
mkdir cvs
cd cvs
cvs -z3 -d:ext:robinrowe@cinepaint.cvs.sourceforge.net:/cvsroot/cinepaint co cinepaint-project
cd cinepaint-project/cinepaint/
./autogen.sh 
./configure 


