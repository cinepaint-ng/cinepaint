#ifndef __GDISPLAY_F_H__
#define __GDISPLAY_F_H__
/* rsr: gimp1.2.4 */
typedef enum
{
  SelectionOff,
  SelectionLayerOff,
  SelectionOn,
  SelectionPause,
  SelectionResume
} SelectionControl;

#endif
