echo ubuntu.sh - install from source on ubuntu
echo License BSD 10.18.2008 Robin.Rowe@cinepaint.org

sudo apt-get install gcc automake g++ libfltk1.1-dev libgtk2.0-dev zlib1g-dev libjpeg62-dev libpng12-dev libtiff4-dev libopenexr-dev libxpm-dev libgutenprint-dev libgutenprintui2-dev liblcms1-dev pkg-config ftgl-dev libxmu-dev libxxf86vm-dev flex python-dev libtool
sh autogen.sh
./configure
make
sudo make install
export LD_LIBRARY_PATH=/usr/lib/local

