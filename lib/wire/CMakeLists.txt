ADD_LIBRARY(wire iodebug.c  
datadir.c   taskswitch.c  wire.c
event.c    libtile.c  
dl_list.c      protocol.c   wirebuffer.c )

MESSAGE("GTK Include: " ${GTK_INCLUDE_DIR})

SET(INC_DIR .. ${FILMGIMP_SRC_DIR} ${GTK_INCLUDE_DIR})

INCLUDE_DIRECTORIES( ${INC_DIR} )

ADD_DEFINITIONS(-DDATADIR=\\\"${PROGRAM_DATA_DIR}\\\" -DVERSION=\\\"${VERSION}\\\" 
                -DDOTDIR=\\\"${programdotdir}\\\" -DPREFIX=\\\"${PREFIX}\\\")
