/* config.h.cmake. Used to generate config.h  */
/* 
   This file is in the public domain.

   Descriptive text for the C preprocessor macros that
   the distributed Autoconf macros can define.
   No software package will use all of them; autoheader copies the ones
   your configure.in uses into your configuration header file templates.

   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  Although this order
   can split up related entries, it makes it easier to check whether
   a given entry is in the file.

   Leave the following blank line there!!  Autoheader needs it.  */
/* Created by Michel Lesoinne on the basis of config.h.in */



#cmakedefine ENABLE_NLS
#cmakedefine HAVE_BIND_TEXTDOMAIN_CODESET
#cmakedefine HAVE_CATGETS
#cmakedefine HAVE_GETTEXT
#cmakedefine HAVE_LC_MESSAGES
#cmakedefine HAVE_DIRENT_H
#cmakedefine HAVE_DOPRNT
#cmakedefine HAVE_IPC_H
#cmakedefine HAVE_NDIR_H
#cmakedefine HAVE_SHM_H
#cmakedefine HAVE_SYS_DIR_H
#cmakedefine HAVE_SYS_NDIR_H
#cmakedefine HAVE_SYS_SELECT_H
#cmakedefine HAVE_SYS_TIME_H
#cmakedefine HAVE_UNISTD_H
#cmakedefine HAVE_VPRINTF
#cmakedefine HAVE_XSHM_H

#cmakedefine IPC_RMID_DEFERRED_RELEASE

#cmakedefine NO_FD_SET

#cmakedefine TIME_WITH_SYS_TIME



/* Leave that blank line there!!  Autoheader needs it.
   If you're adding to this file, keep in mind:
   The entries are in sort -df order: alphabetical, case insensitive,
   ignoring punctuation (such as underscores).  */

/* Define to one of `_getb67', `GETB67', `getb67' for Cray-2 and Cray-YMP
   systems. This function is required for `alloca.c' support on those systems.
   */
#cmakedefine CRAY_STACKSEG_END


/* Define if using `alloca.c'. */
#cmakedefine C_ALLOCA

/* Define to 1 if translation of program messages to the user's native
   language is requested. */
#cmakedefine ENABLE_NLS

/* Define if you have `alloca', as a function or macro. */
#cmakedefine HAVE_ALLOCA

/* Define if you have <alloca.h> and it should be used (not on Ultrix). */
#cmakedefine HAVE_ALLOCA_H

/* Define to 1 if you have the `dcgettext' function. */
#cmakedefine HAVE_DCGETTEXT

/* Define if you have the <dirent.h> header file, and it defines `DIR'. */
#cmakedefine HAVE_DIRENT_H

/* Define if you have the <dlfcn.h> header file. */
#cmakedefine HAVE_DLFCN_H

/* Define if you don't have `vprintf' but do have `_doprnt.' */
#cmakedefine HAVE_DOPRNT

/* Define if the GNU gettext() function is already present or preinstalled. */
#cmakedefine HAVE_GETTEXT

/* Define if you have the <inttypes.h> header file. */
#cmakedefine HAVE_INTTYPES_H

/* Define if you have <langinfo.h> and nl_langinfo(CODESET). */
#cmakedefine HAVE_LANGINFO_CODESET

/* Define if your <locale.h> file defines LC_MESSAGES. */
#cmakedefine HAVE_LC_MESSAGES

/* Define if you have the `tiff' library (-ltiff). */
#cmakedefine HAVE_LIBTIFF

/* Define if you have the <memory.h> header file. */
#cmakedefine HAVE_MEMORY_H

/* Define if you have the <ndir.h> header file, and it defines `DIR'. */
#cmakedefine HAVE_NDIR_H

/* Define if you have the <stdint.h> header file. */
#cmakedefine HAVE_STDINT_H

/* Define if you have the <stdlib.h> header file. */
#cmakedefine HAVE_STDLIB_H

/* Define if you have the <strings.h> header file. */
#cmakedefine HAVE_STRINGS_H

/* Define if you have the <string.h> header file. */
#cmakedefine HAVE_STRING_H

/* Define if you have the <sys/dir.h> header file, and it defines `DIR'. */
#cmakedefine HAVE_SYS_DIR_H

/* Define if you have the <sys/ndir.h> header file, and it defines `DIR'. */
#cmakedefine HAVE_SYS_NDIR_H

/* Define if you have the <sys/stat.h> header file. */
#cmakedefine HAVE_SYS_STAT_H

/* Define if you have the <sys/types.h> header file. */
#cmakedefine HAVE_SYS_TYPES_H

/* Define if you have <sys/wait.h> that is POSIX.1 compatible. */
#cmakedefine HAVE_SYS_WAIT_H

/* Define if you have the <unistd.h> header file. */
#cmakedefine HAVE_UNISTD_H

/* Define if you have the `vprintf' function. */
#cmakedefine HAVE_VPRINTF

/* Name of package */
#define PACKAGE "@PACKAGE@"

/* Define as the return type of signal handlers (`int' or `void'). */
#define RETSIGTYPE @REGSIGTYPE@

/* If using the C implementation of alloca, define if you know the
   direction of stack growth for your system; otherwise it will be
   automatically deduced at run-time.
        STACK_DIRECTION > 0 => grows toward higher addresses
        STACK_DIRECTION < 0 => grows toward lower addresses
        STACK_DIRECTION = 0 => direction of growth unknown */
#cmakedefine STACK_DIRECTION

/* Define if you have the ANSI C header files. */
#cmakedefine STDC_HEADERS

/* Define if you can safely include both <sys/time.h> and <time.h>. */
#cmakedefine TIME_WITH_SYS_TIME

/* Version number of package */
#define VERSION "@VERSION@"

/* Define if your processor stores words with the most significant byte first
   (like Motorola and SPARC, unlike Intel and VAX). */
#cmakedefine WORDS_BIGENDIAN

/* Define as `__inline' if that's what the C compiler calls it, or to nothing
   if it is not supported. */
#cmakedefine inline

/* Define to `int' if <sys/types.h> does not define. */
#cmakedefine pid_t
