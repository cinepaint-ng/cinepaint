# /usr/bin/sh
# Robin.Rowe@MovieEditor.com 6/2/03, as suggested by Simon Watts

for F in `find . -name '*\.[hc]' -o -name '*\.cpp' -o -name '*\.am' -o -name '*\.in'`
do
echo $F
cat $F | tr -d '\r' > $F.tmp && mv -f $F.tmp $F
done

