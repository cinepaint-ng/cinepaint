README-CMAKE
Copyright 2008.03.16 Robin.Rowe@CinePaint.org
License: BSD

cd cinepaint-project
cmake .
make
make install

***Notes from Michel Lesoinne <michel.lesoinne@gmail.com>

With the exception of openexr, all the plug-ins are configured inside the plug-ins/CMakeList.txt

A simple example is:

ADD_EXECUTABLE(sgi sgi/sgi.c  sgi/sgilib.c)
TARGET_LINK_LIBRARIES(sgi lib wire ${GTK_LIBRARIES})

So typically, the first line defines the name of the plug-in executable and lists all source files. The second line lists the libraries. The format is really compact. in that line for example, "lib" represents liblib.a and wire represents libwire.a.

Most plugins will require also the gtk libraries which have been located by a module and they are in the obvious variable ${GTK_LIBRARIES}.

Finally at the end, you will see:

INSTALL(TARGETS blur bmp cineon compose decompose dicom edge fits
                gauss_rle gbr gifload hdr iff jpeg png pnm sgi
        DESTINATION ${PROGRAM_PLUGINS_DIR_INFIX})

Which says to install all the plugins executable into the plugin installation directory.

How about the include, will you ask? Well, the includes that are generally sufficient for most plug-ins are listed at the top of the file in the line:

SET(INC_DIR . ${GTK_INCLUDE_DIR} ${FILMGIMP_SRC_DIR}
            ${FILMGIMP_SRC_DIR}/lib)

INCLUDE_DIRECTORIES( ${INC_DIR} )

You could do this in a single command. The only reason I use two is that it will be easier later if one needs to add a few includes that are system specific or that depend on a configuration.

The includes are the reason I did openexr separately, because it requires some includes that other plug-ins do not require and I did not pollute the include list of directories
with them. So openexr is added to the target list with:

ADD_SUBDIRECTORY(openexr)

which means that the openexr subdirectory will also contain its own CMakeLists.txt with similar entries to what I just described.

INSTALLATION:

Looking at the installation that the "configured" based filmgimp installs, I see a bunch of directories I have not even touched: curves, gradients, palettes, patterns, scripts, iol  and then a few files: gimprc_user, gtkrc, gtkrc.forest2, printrc_user, ps-menurc, spot.splash.ppm. If any of the files listed at the end needs to be configured, I can do it. The configuration
command is wonderfully easy and powerfull. In fact some of the stuff I did to create user_install would be a lot simpler if I had known about it before. I will clean it up soon.

BUILD-TIME CONFIGURATION:

cmake -i  or ccmake which will bring up a form to be modified. This will go through every variable that was defined during the last cmake and will allow you to modify them. One of them is CMAKE_INSTALL_PREFIX and you can set it to what you want. I am not sure if you have to run cmake once before, with just "cmake ." or if it can be the first cmake you do.

Note that I had to commit some small changes to cvs to make this work, I had ${PROGRAM_PLUGINS_DIR} instead of ${PROGRAM_PLUGINS_DIR_INFIX} in the installation command and
another tiny detail. I think it is still not fully correct though as I think the program
still looks for the plugins in /usr/local.

###